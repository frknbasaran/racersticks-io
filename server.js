/*
@author     : Furkan BAŞARAN
@project    : Racersticks-IO
@start-date : 02.06.2014
*/
var app = require('express')(),
    server = require('http').createServer(app),
    io = require('socket.io').listen(server),
    port = 8080,
    url  = 'http://localhost:' + port + '/';

/* nodejitsu settings */
if(process.env.SUBDOMAIN){
  url = 'http://' + process.env.SUBDOMAIN + '.jit.su/';
}

server.listen(port);
console.log("Express server listening on port " + port);
console.log(url);

app.configure(function(){
    app.use(express.static(path.join(__dirname,'public')));
});
app.get('/', function (req, res) {
  res.sendfile(__dirname + '/index.html');
});

/* active users and clients arrays */
var activeUsers = {};
var onlineClients = {};

// when someone connect to server
io.sockets.on('connection', function (socket) {
  
  // listen "construct" request from client
  socket.on('construct',function(data){
        socket.username = data.username;
		socket.userId	= activeUsers.length;
        socket.height   = data.height;
		activeUsers[data.username] = {
			activeUsername   : data.username,
			activeUserId     : activeUsers.length,
            userID           : socket.id,
            activeUserHeight : data.height
        };
        // send notify when user connected
        io.sockets.emit("racers",{message:"<b>"+socket.username+"</b> connected to race"});
        onlineClients[data.username] = socket.id;
        io.sockets.emit("update", activeUsers);
  });
  
  socket.on('heightChange',function(data){
        // update users score on server-side
        activeUsers[data.username].activeUserHeight = parseInt(activeUsers[data.username].activeUserHeight)+1;
        // check score and update status
        if(activeUsers[data.username].activeUserHeight>399){
            io.sockets.emit("win",{username:data.username});
        }
        else io.sockets.emit("update", activeUsers);
  });
    
  // when users disconnected send notify to client    
  socket.on('disconnect',function(){
    io.sockets.emit("racers",{message:"<b>"+socket.username+"</b> disconnected from race"});
    delete activeUsers[socket.username];
    io.sockets.emit("update", activeUsers);
  });
    
});