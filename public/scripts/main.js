  $(function(){
            // colors array
            var color = ["#34495e","#f39c12","#c0392b","#8e44ad","#2ecc71","#e67e22","#3498db","#c0392b"];
            // connect to socket.io
            var socket = io.connect();
            // get username
            var username = prompt('username?');
            // define default values to variables
            var h=1;
            var id;
            var finish = false;
            var gz;
            
            // constructer function send pocket to server
            socket.emit('construct',{username: username,height:h});
            
            // when clicked mouse function
            $('html').click(function(){
                    h++;
                    var data = {username:username};
                    if(!finish) send_pocket(data);
            });
              
            // update server
            var send_pocket = function(data){
                socket.emit('heightChange',data);
            }
            
            // notify div scroll 
            $('.notify').slimScroll({height:"400px"});
            
            // update heights data from server
            socket.on('update', function (data) {
                $('.main_area').empty();
                $('.users_area').empty();
                gz=0;
                $.each(data,function(key,val){
                    $('.users_area').append('<i class="user">'+val.activeUsername+'</i>');
                    $('.main_area').append('<div class="stick '+val.userID+'">'+val.activeUserHeight+'</div>');    
                    $('.'+val.userID).css('height',val.activeUserHeight);
                    $('.'+val.userID).css('background-color',color[gz]);
                    gz++;
                });
            });
            
            // if user win show notice
            socket.on("win",function(data){
                finish = true;
                $('.notify').append("winner <b>"+data.username+'</b>!!');
            });
            
            // when users connect or disconnect show notify
            socket.on("racers",function(data){
                $('.notify').append(data.message+'<br>');
            });  
          });
        